<?php
	session_start();
	// had to suppress the notices cuz was giving notices error when xml file was extended
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set('display_errors', 1);
	
	$flag = 0;	
	$quantity;
	$typename;
	$price;
	foreach($_POST as $key => $value)
	{
		//array to save category name, its size, price and quantity of each item
		$cart = [];
		
		//for those where something was submitted
		if(!empty($value))
		{
			$quantity = $value;
			
			//checking if input was a number or numeric string if else display error
			if (!is_numeric($quantity))
			{
				$flag = 1;
			}	
			else
			{
				//breaking the input name to get the type thats in xml from input name, the section before '#' 
				$typename = strstr($key, "#", true);
			
				//getting the part that comes after '#'
				$price = substr($key, strpos($key, "#") + 1);
			
				//now converting the '_' to a space and '.'
				$typename = str_replace("_", " ", $typename);
				$price = str_replace("_", ".", $price);
			
				//checking if '&' in name is present then convert if to &amp;
				/*if(strpos($typename, "&"))
				{
					$typename = str_replace("&", "&amp;", $typename);
				}*/	
			

				//opening the xml file to search
				$dom = simplexml_load_file("../menu.xml");
			
				//searching
				foreach($dom->xpath("/menu/category") as $category)
				{
					//iterating over every name tag in xml
					foreach($category->name as $name)
					{
					
						//checking every type tag
						if($name->type == $typename)
						{
							//storing what category name the type belongs to.Needed to convert to string because without it giving error
							$cartcategoryname = (string)$category->option;
												
							//test to see what size the above matched type is
							if($name->small == $price)
							{
								$size = "small";
							}
							else if($name->large == $price)
							{
								$size = "large";
							}
							else
							{
								$size = "general";
							}
						}
										
					}
				
					/*adding the name,size,type quantity in an array
					* which would be added to $_SESSION and the array
					* would be wiped clean.*/ 
					$cart = [
						"category" => $cartcategoryname,
						"type" => $typename,
						"size" => $size,
						"price" => $price,
						"quantity" => $quantity
						];	
					
					
				}	
			
				$_SESSION["cart"][] = $cart;
				
			}
		}	
		
				
	}
	//print_r($_SESSION);
	//determining which output to display based on flag
	if ($flag == 1)
	{
		include("../views/header.php");
		include("../views/invalidinput.php");
	}	
	else
	{
		// redirecting to home page	
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'index.php?cat=1';
		header("Location: http://$host$uri/$extra");
		exit;
	}	
	
	
?>
