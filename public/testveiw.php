<!DOCTYPE html>
<html>
	<head>
		<title>Test View page</title>
		<style>
			body
			{
				font-family : 	Arial, Verdana, sans-serif;
				color : #fffaf0;
				background-color: #000;
			}	
			table
			{
				width: 800px;
				border-collapse: collapse;
				margin: 10px auto 10px auto;
			}
			td, th
			{
				padding: 7px 10px 10px 10px;
				
			}
			th
			{
				text-transform: uppercase;
				border-bottom: 1px solid #f8f8ff;
				text-align: center;
				background-color: #e6e6fa;
				color: #808080;
				
			}
			tr
			{
				background-color: #a9a9a9;
				text-align: center;
			}
			tr.even
			{
				background-color: #4d4d4d;	
			}
			input[type = "text"]
			{
				width: 13px;
				margin-left: 5px;
			}
			input[type = "submit"]
			{
				padding: 5px 10px 5px 10px;
				font-size: 18px;
				color:#fff;
				width: 85px;
				border-radius: 6px;
				background-color: #daa520;
				border: none;
				margin-left: 700px;
			}
			input[type = "submit"]:focus
			{
				display: pointer;
				background-color: #f2dca6;
			}
			input[type = "submit"]:hover
			{
				cursor: pointer;	
			}
			.list
			{
				
				margin: 10px 250px 10px 250px;
				color: #808080;
				text-align:center;	
			}	
			ul
			{
				
				text-align: center;
				list-style-type: none;
			}
			li
			{
				
				float:left;
				margin-right: 10px;
				
					
			}
			a
			{
				text-decoration: none;
				color : #a4c0f4;
			}			
				
		</style>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th colspan = "4">Pizzas</th>
				</tr>
				<tr>
					<th>Options</th>
					<th>Small</th>
					<th>Large</th>
					<th>General Price</th>
				</tr>
			</thead>
			<tbody>
				<tr class = "even">
					<td>Tomatao &amp; Cheeze</td>
					<td>$ 5.85<input type = "text" name = "Tomato &amp; Cheese small"></input></td>
					<td>$ 6.85<input type = "text" name = "Tomato &amp; Cheese large"></input></td>
					<td></td>
				</tr>
				<tr>
					<td>Onions</td>
					<td>$ 6.85<input type = "text" name = "Onions small"></input></td>
					<td>$ 10.85<input type = "text" name = "Onions large"></input></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<form action = "buy.php" method = "post">
			<input type = "submit" value = "Buy!">
		</form>
		<div class = "list">
			<h3>Category</h3>
			<ul>
				<li><a href= "#">1</a></li>
				<li><a href= "#">2</a></li>
			</ul>
		</div>
	</body>
</html>
