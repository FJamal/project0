<!DOCTYPE html>
<html>
	<head>
		<title>Test View page</title>
		<style>
			body
			{
				font-family : 	Arial, Verdana, sans-serif;
				color : #fffaf0;
				background-color: #000;
			}	
			table
			{
				width: 800px;
				border-collapse: collapse;
				margin: 10px auto 10px auto;
			}
			td, th
			{
				padding: 7px 10px 10px 10px;
				
			}
			th
			{
				text-transform: uppercase;
				border-bottom: 1px solid #f8f8ff;
				text-align: center;
				background-color: #e6e6fa;
				color: #808080;
				
			}
			tr
			{
				background-color: #a9a9a9;
				text-align: center;
			}
			tr.even
			{
				background-color: #4d4d4d;	
			}
			input[type = "text"]
			{
				width: 13px;
				margin-left: 5px;
			}
			input[type = "submit"]
			{
				padding: 5px 10px 5px 10px;
				font-size: 18px;
				color:#fff;
				width: 85px;
				border-radius: 6px;
				background-color: #daa520;
				border: none;
				margin-left: 700px;
			}
			input[type = "submit"]:focus
			{
				display: pointer;
				background-color: #f2dca6;
			}
			input[type = "submit"]:hover
			{
				cursor: pointer;	
			}
			.list
			{
				
				margin: 10px 250px 10px 250px;
				color: #808080;
				text-align:center;	
			}	
			ul
			{
				
				text-align: center;
				list-style-type: none;
			}
			li
			{
				
				float:left;
				margin-right: 10px;
				
					
			}
			a
			{
				text-decoration: none;
				color : #a4c0f4;
			}
			input[type = "submit"].remove
			{
				font-size: 12px;
				color:#fff;
				width: 55px;
				margin-left:0px;
				padding: 5px 5px 5px 5px;
			}
			input[type = "submit"].checkout
			{
				margin-left:5px;
				padding: 5px 3px 5px 3px;
				float: right;
					
			}
			.total
			{
				background-color: #a9a9a9;
				font-size: 20px;
				color: #fffaf0;
				text-align:left;
				border-top: 2px solid #f8f8ff;
				border-bottom: none;
			}
			.totalp 
			{
				font-size: 20px;
				text-align: left;
				border-top: 2px solid #f8f8ff;
				
			}							
				
		</style>
	</head>
	<body>
		<div class = "cart">
			<a href= "cart.php">View Menu</a>
		</div>
		
		<table>
			<thead>
				<tr>
					<th colspan = "6">Your Cart!</th>
				</tr>
				<tr>
					<th>Category</th>
					<th>With Options</th>
					<th>Size</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Total</th>
				</tr>
			</thead>
			
			<tbody>
				<form action = "cart.php" method = "POST">
				<tr>
					<td>Category Name</td>
					<td>Option name</td>
					<td>Small</td>
					<td>$4.50</td>
					<td>
						<input type = "text" value = "4">
					</td>
					<td>18.50 <input class= "remove" type = "submit" name = "som" value = "Remove"></td>
					
				</tr>
				<tr>
					<th class = "total"  colspan = '5'>GRAND TOTAL</th>
					<td class = "totalp">$50000</td>
				</tr>
			</tbody>
		</table>
		<div class = "checkoutsection">
		<input class = "checkout" type = "submit" name = "checkout" value = "Checkout">
		<input class = "checkout" type = "submit" name = "update" value = "Update"> 
		</div>
		</form>
		
	</body>
</html>
