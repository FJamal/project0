<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	//loading the xml file
	$dom = simplexml_load_file("../menu.xml");
	
	// variable to store category id
	$categoryid;
	
	//to store all the data of a particular category id
	$names = [];
	
	//to store the name of the category
	$categoryname;
	
	//to count how many categories were in there in the xml file
	$categorycount = count($dom->xpath("/menu/category"));
	
	//Checking if the user visited via GET and if GET variable is set
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		
		if(!isset($_GET["cat"]))
		{
			$categoryid = 1;
		}
		// checking if the variable is set within the limits of number of categories loaded
		else if (isset($_GET["cat"]) && ($_GET["cat"] > 0 && $_GET["cat"] <= $categorycount))
		{
			$categoryid = $_GET["cat"];
		}
		else
		{
			print("invalid category id");
		}
		
		//now loading the appropriate category from xml
		foreach($dom->xpath("/menu/category[@id = '{$categoryid}']") as $category)
		{
			$categoryname = $category->option;
			
			//saving the data
			foreach($category->name as $name)
			{
				$names[] = [
						"optionname" => $name->type,
						"small" => $name->small,
						"large" => $name->large,
						"general" => $name->general
						];
			}	
		}
		
		//loading the displaying files
		include("../views/header.php");
		include("../views/viewpage.php");

					
	}
		
		
?>

