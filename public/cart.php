<?php
	session_start();
	//suppress notices use ^ E_NOTICE
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	// calculating the total for every category
	//if(isset($_SESSION["cart"]))
	//{
		//$sessioncount = (count($_SESSION["cart"]));
	//}
		
	//if visited through GET than updating the sessions
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		if(isset($_SESSION["cart"]))
		{
			$sessioncount = (count($_SESSION["cart"]));
		}
		else
		{
			$sessioncount = 0;
		}	
		//if $_SESSION["cart"] is set
		if(isset($_SESSION["cart"]))
		{	
			
			for($i = 0; $i < $sessioncount; $i++)
			{
				$quant = $_SESSION["cart"][$i]["quantity"];
				$itsprice = $_SESSION["cart"][$i]["price"];
		
				//formatting the total in 2 decimal place
				$totalprice = number_format(((float)$quant * (float)$itsprice), "2", ".", "");
		
				//adding the total of each category back to the session
				$_SESSION["cart"][$i]["total"] = $totalprice;
			}
			
			//calculating the grand total which would be used in display file
			$sum = 0;
			for($i = 0; $i < $sessioncount; $i++)
			{
				$sum = $sum + $_SESSION["cart"][$i]["total"]; 
				$sum = number_format($sum, "2", ".", "");
			}	
		}
		//loading the views
		include("../views/header.php");
		include("../views/cartview.php");
		
	}	
	
	//if method was post means either a remove button or update or checkout was clicked
	if($_SERVER["REQUEST_METHOD"] = "POST")
	{
		if(isset($_SESSION["cart"]))
		{
			$sessioncount = (count($_SESSION["cart"]));
		}
		else
		{
			$sessioncount = 0;
		}
			
		// any of the remove button was clicked
		if(!isset($_POST["checkout"]) && !isset($_POST["update"]))
		{
			//checking which remove button was clicked based on their names as "remove$i"
			for($i = 0; $i < $sessioncount; $i++)
			{
				if(isset($_POST["remove{$i}"]))
				{
					//that means thats the elemet to delete
					array_splice($_SESSION["cart"], $i, 1);
					
					//after deletion redirecting to cart.php via GET
					$host  = $_SERVER['HTTP_HOST'];
					$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
					$extra = 'cart.php';
					header("Location: http://$host$uri/$extra");
					exit;
				}	
			}	
		}
		
		//if update or checkout button was clicked
		if(isset($_POST["checkout"]) || isset($_POST["update"]))
		{
			//checking for update button
			if(isset($_POST["update"]))
			{
				//checking which input field was changed if any based on their names "quantity$i"
				for($i = 0; $i < $sessioncount; $i++)
				{
					if(isset($_POST["quantity{$i}"]))
					{
						if($_SESSION["cart"][$i]["quantity"] != $_POST["quantity{$i}"])
						{
							//if the value is different from that stored in session means value was changed and now save the new value
							$_SESSION["cart"][$i]["quantity"] = $_POST["quantity{$i}"];
							
						}	
					}	
				}
				
				//redirecting to cart
				$host  = $_SERVER['HTTP_HOST'];
				$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
				$extra = 'cart.php';
				header("Location: http://$host$uri/$extra");
				exit;	
			}
			
			//if user clicked checkout inform him the order been processed
			if(isset($_POST["checkout"]))
			{
				//loading the views to inform order processed
				include("../views/header.php");
				include("../views/checkout.php");
			}		
		}
		
				
	}	
	
		
	
?>
