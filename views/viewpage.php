<body>
	<div class = "cart">
		<a href= "cart.php">View Cart</a>
	</div>
	<?php if(isset($error)): ?>
	<div class = "warning">
		<p>No Item was selected</p>
	</div>
	<?php endif; ?>
		<table>
			<thead>
				<tr>
					<th colspan = "4"><?= $categoryname ?></th>
				</tr>
				<tr>
					<th>Options</th>
					<th>Small</th>
					<th>Large</th>
					<th>General Price</th>
				</tr>
			</thead>
			<tbody>
				<form action = "buy.php" method = "POST">
					<?php foreach($names as $item): ?>
					<tr class = "even">
						<td><?= $item["optionname"] ?></td>
						<td>
							<!-- checking every key value pair of $item if its empty and dynamically giving the names to inputs combining name and price-->
							<?php if($item["small"] != ""): ?>
							<?= $item["small"] ?>
							<input type = "text" name = "<?= $item["optionname"]?>#<?=$item["small"]?>">
							</input>
							<?php endif; ?>
						</td>
						<td>
							<?php if($item["large"] != ""): ?>
							<?= $item["large"] ?>
							<input type = "text" name = "<?= $item["optionname"]?>#<?= $item["large"] ?>">
							</input>
							<?php endif; ?>
						</td>
						<td>
							<?php if($item["general"] != ""): ?>
							<?= $item["general"] ?>	
							<input type = "text" name = "<?= $item["optionname"]?>#<?= $item["general"] ?>">
							</input>	
							<?php endif; ?>	
						</td>
					</tr>
					<?php endforeach ?>
				
			</tbody>
		</table>
		
			<input type = "submit" value = "Buy!">
		</form>
		
		<!-- printing the list to display categories using the categorycount variable-->
		<div class= "list">
			<h4>category</h4>
			<?php
				for($i = 1; $i <= $categorycount; $i++)
				{
					print("<ul>
							<li><a href = \"?cat={$i}\">{$i}</a></li>
						  </ul>");
				}	
			?>
		</div>	
	</body>
</html>
