<body>
		<div class = "cart">
			<a href= "index.php">View Menu</a>
		</div>
		
		<table>
			<thead>
				<tr>
					<th colspan = "6">Your Cart!</th>
				</tr>
				<tr>
					<th>Category</th>
					<th>With Options</th>
					<th>Size</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Total</th>
				</tr>
			</thead>
			<?php if(!isset($_SESSION["cart"]) || count($_SESSION["cart"]) < 1): ?>
				<div><p>No items to display in your cart</p></div>
			<?php endif; ?>
			
			<!-- if $_SESSION is set than display the table if count not added than update/checkout button is shown when cart is emptied using remove button -->
			<?php if(isset($_SESSION["cart"]) && count($_SESSION["cart"]) > 0): ?>
				<tbody>
					<form action = "cart.php" method = "POST">
					<!-- outputting items selected from SESSION -->
					<?php for($i = 0; $i < $sessioncount; $i++): ?>
					<tr>
						<td><?= $_SESSION["cart"][$i]["category"] ?></td>
						<td><?= $_SESSION["cart"][$i]["type"] ?></td>
						<td><?= $_SESSION["cart"][$i]["size"] ?></td>
						<td>$<?= $_SESSION["cart"][$i]["price"] ?></td>
						<td>
							<!-- displaying quantity as value and giving its name w $i so cart.php can check if value was updated -->
							<input type = "text" name ="quantity<?= $i ?>" value = "<?= $_SESSION["cart"][$i]["quantity"] ?>">
						</td>
						<!-- adding $i in the name of remove buttons in order to track them in cart.php and remove that particular category -->
						<td>$<?= $_SESSION["cart"][$i]["total"] ?><input class= "remove" type = "submit" name = "remove<?= $i ?>" value = "Remove"></td>
					
					</tr>
					<?php endfor; ?>
					<!-- including the grand total -->
					<tr>
						<th class = "total"  colspan = '5'>GRAND TOTAL</th>
						<td class = "totalp">$<?= $sum ?></td>
					</tr>
				</tbody>
			</table>
			<div class = "checkoutsection">
			<input class = "checkout" type = "submit" name = "checkout" value = "Checkout">
			<input class = "checkout" type = "submit" name = "update" value = "Update"> 
			</div>
			</form>
			<?php endif; ?>
				
	</body>
</html>
