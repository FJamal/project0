<!DOCTYPE html>
<html>
	<head>
		<title>My Menu page</title>
		<style>
			body
			{
				font-family : 	Arial, Verdana, sans-serif;
				color : #fffaf0;
				background-color: #000;
			}	
			table
			{
				width: 800px;
				border-collapse: collapse;
				margin: 10px auto 10px auto;
			}
			td, th
			{
				padding: 7px 10px 10px 10px;
				
			}
			th
			{
				text-transform: uppercase;
				border-bottom: 1px solid #f8f8ff;
				text-align: center;
				background-color: #e6e6fa;
				color: #808080;
				
			}
			tr
			{
				background-color: #a9a9a9;
				text-align: center;
				border-bottom: 1px solid #f8f8ff;
			}
			tr.even
			{
				background-color: #4d4d4d;	
			}
			input[type = "text"]
			{
				width: 13px;
				margin-left: 5px;
			}
			input[type = "submit"]
			{
				padding: 5px 10px 5px 10px;
				font-size: 18px;
				color:#fff;
				width: 85px;
				border-radius: 6px;
				background-color: #daa520;
				border: none;
				margin-left: 700px;
			}
			input[type = "submit"]:focus
			{
				display: pointer;
				background-color: #f2dca6;
			}
			input[type = "submit"]:hover
			{
				cursor: pointer;	
			}
			.list
			{
				
				margin: 10px 250px 10px 250px;
				color: #808080;
				text-align:center;	
			}	
			ul
			{
				
				text-align: center;
				list-style-type: none;
			}
			li
			{
				
				float:left;
				margin-right: 10px;
				
					
			}
			a
			{
				text-decoration: none;
				color : #a4c0f4;
			}
			.cart
			{
				float: right;	
			}
			input[type = "submit"].remove
			{
				font-size: 12px;
				color:#fff;
				width: 55px;
				margin-left:0px;
				padding: 5px 5px 5px 5px;
			}
			input[type = "submit"].checkout
			{
				margin-left:5px;
				padding: 5px 3px 5px 3px;
				float: right;
					
			}
			.total
			{
				background-color: #a9a9a9;
				font-size: 20px;
				color: #fffaf0;
				text-align:left;
				border-top: 2px solid #f8f8ff;
				border-bottom: none;
			}
			.totalp 
			{
				font-size: 20px;
				text-align: left;
				border-top: 2px solid #f8f8ff;
				border-bottom: none;
				
			}	
				
		</style>
	</head>
